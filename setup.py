#!/usr/bin/env python

from setuptools import setup
from sys import path

setup(name= 'pylibhack',
    version= '0.1.0',
    author= '@sha0coder',
    author_email= 'sha0@badchecksum.net',
    py_modules= ['pylibhack'],
    #url= 'https://github.com/elssar/conundrum',
    license= 'GPL',
    #package_dir = {'.': 'module'},
    description= 'hacking abstractions',
    packages = ['pylibhack','pylibhack.crypt','pylibhack.db','pylibhack.file','pylibhack.ia'],
    #long_description= open(path[0]+'/README.md', 'r').read(),
    '''
    install_requires= [
        'PyYAML >= 3.0.9',
        'Markdown >= 2.2.0',
        'requests >= 1.0.4',
        ],
    '''
)
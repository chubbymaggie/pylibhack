'''
    safe sanitizations and filters
'''
import re



def sql(data):
    return re.sub('[\'"%]','',str(data))

def html(data):
    return data.replace('<','&lt;').replace('>','&gt;').replace('"','&quot;').replace("'","&#39;").replace('&','&amp;')

def cmd(data):
    return re.sub('[!|`$();<>\\\]','',data)

def url(data):
    return cmd(sql(data))

def paranoid(data):
    return cmd.sub('[^a-zA-Z0-9_,/.<>-]','',data)




#!/usr/bin/env python2
'''
	URL() url parsing by @sha0coder
'''


import re
import sys


class URL:

	def __init__(self,url=None):
		self.url = url
		self.base = ''
		self.proto = 'http'
		self.ssl = False
		self.hostport = ''
		self.host = ''
		self.port = 80
		self.path = '/'
		self.params = ''

		if url:
			self.parse(url)

	def __str__(self):
		out  = 'url: %s\n' % self.url       #TODO: mejor con una estructura
		out += 'base: %s\n' % self.base
		out += 'proto: %s\n' % self.proto
		out += 'ssl: %s\n' % self.ssl
		out += 'hostport: %s\n' % self.hostport
		out += 'host: %s\n' % self.host
		out += 'port: %s\n' % self.port
		out += 'path: %s\n' % self.path
		out += 'params: %s\n' % self.params
		return out


	def getUrl(self):
		return self.url
	def getBase(self):
		return self.base
	def isSSL(self):
		return self.ssl
	def getProtocol(self):
		return self.proto
	def getHost(self):
		return self.host
	def getDomain(self):
		return self.host
	def getPort(self):
		return self.port
	def getPath(self):
		return self.path
	def getParams(self):
		return self.params

	def parse(self,url):
		try:
			url = self.fixUrl(url)
			self.url = url
			parts = url.split('/')

			self.proto = parts[0][:-1]
			self.ssl = (self.proto == 'https')
			if self.ssl:
				self.port = 443
			else:
				self.port = 80

			self.hostport = parts[2]
			hostparts = self.hostport.split(':')
			self.host = hostparts[0]
			if len(hostparts) > 1:
				self.port = int(hostparts[1])

			self.base = '%s://%s' % (self.proto, self.hostport)
			self.path = self.url.replace(self.base,'')
			parts = self.path.split('?')
			self.path = parts[0]
			self.params = parts[1]
		except:
			pass

	def endswith(self,s):
		return self.url.endswith(s)

	def isDirectory(self):
		return self.url.endswith('/')

	def getDirectory(self):
		return re.sub('/[^/]+$','/',self.path) #TODO: compile


	def getSubUrls(self):
		return self._getSubUrls(self.path)

	def _getSubUrls(self,url):
		suburls = []
		prev = '/'
		url = self.fix(url)[1:]

		spl = url.split('/')
		for i in range(0,len(spl)):
			if spl[i] != '':
				prev += spl[i]

				if i+1 < len(spl): # not last
					prev += '/'
				
				suburls.append(URL(self.base+prev))

		
		'''
		for i in range(0,len(spl)):
			if i+1 == len(spl) and spl[i] == '':
				break

			prev += '/'+spl[i]
			if i+1 == len(spl):
				suburls.append(prev)
			else:
				suburls.append(prev+'/')
		'''

		return suburls


	def fix(self,url):
		url = re.sub('(\?|%3f).*','',url)
		url = url.replace('\x0d','%0d').replace('/./','/').replace('/\.\./','/').replace('#','')
		url = re.sub('//+','/',url)

		if url[0] != '/':
			url='/'+url


			

		return url

	def getDirBase(self,url):
		return re.sub('/[^/]+$','/',url) #TODO: compile


	def fixUrlDir(self,url):
		url = self.fixUrl(url)
		if url[-1] != '/':
			url += '/'
		return url

	def fixUrl(self,url):
		url = url.replace('\x0d','%0d').replace('/./','/').replace('/\.\./','/').replace('#','');
		if not url.startswith('http'):
			url = 'http://'+url

		i = url.find('//')            #TODO: optimizar
		i = url.find('//',i+1)
		while i>=0:
			url = url[:i]+url[i+1:]
			i=url.find('//',i+1)
			
		return url


'''
if __name__ == '__main__':
	furl = URL()

	url = 'aa/bb/cc/'
	print furl.getSubUrls(url)


	url = 'aa/bb/cc.php'
	print furl.getSubUrls(url)

	url = 'aa.php'
	print furl.getSubUrls(url)

	url = 'aa'
	print furl.getSubUrls(url)

	print '---------------'

	u = [
		'http://lala.com/a-bb/cc.com',
		'http://lala.com/a-bb/cc.com?id=a&name=foo',
		'https://coco.com/asdf/',
		'http://coco.net',
		'http://tralara.net:33/a-bb/cc.com?id=a&name=foo',
		'https://tralara.net:33/a-bb/cc.com?id=a&name=foo',
	]

	for url in u:
		print URL(url)
'''

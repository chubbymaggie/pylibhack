
__version__ = "$Revision: 0004000 $"


from threading import Thread
import mimetypes
import httplib
import httplib2
import socket
import time
from url import URL


class HTTP2:

    def __init__(self):
        socket.setdefaulttimeout(30)
        self.http = httplib2.Http()
        self.timeout = 20
        self.headers = {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "text/plain",
            "User-agent": "Mozilla/5.0 (Windows NT 6.1; rv:20.0) Gecko/20100101 Firefox/20.0"
        }
        self.errors = 0

    def setUserAgent(self,ua):
        self.headers["User-agent"] = ua

    def setKeepalive(self,):
        self.headers.update({"Connection": "keepalive"})

    def setHeader(self,k,v):
        self.headers.update({k,v})

    def check(self,url):
        hdrs, _ = self.get(url)
        if hdrs.status != 404:
            return True
        else:
            return False
        
    def get(self,url):
        try:
            hdrs, html = self.http.request(url, 'GET', headers=self.headers, redirections=3)
            return hdrs, html
        except socket.timeout:
            return None, ''
        except httplib2.RedirectLimit:
            return None, ''
        except:
            return None, ''       
        
    def post(self,url,cmd):
        #TODO: refactor to return hdrs,html instead of html,hdrs (modify all xplts)
        try:
            cmd = cmd.replace(' ','%20')
            hdrs,html = self.http.request(url, 'POST', headers=self.headers, body=cmd, redirections=3)
            return html,hdrs
        except socket.timeout:
            return '', None
        except httplib2.RedirectLimit:
            return '', None
        except:
            return '', None

'''
    TOFIX:
    
    def multipart(self, host, selector, fields, files):
        content_type, body = encode_multipart_formdata(fields, files)
        h = httplib.HTTP(host)
        h.putrequest('POST', selector)
        h.putheader('content-type', content_type)
        h.putheader('content-length', str(len(body)))
        for k in headers.keys():
            h.putheader(k,headers[k])
        h.endheaders()
        h.send(body)
        errcode, errmsg, headers = h.getreply()
        return h.file.read()

    def encode_multipart_formdata(self, fields, files):
        LIMIT = '----------lImIt_of_THE_fIle_eW_$'
        CRLF = '\r\n'
        L = []
        for (key, value) in fields:
            L.append('--' + LIMIT)
            L.append('Content-Disposition: form-data; name="%s"' % key)
            L.append('')
            L.append(value)
        for (key, filename, value) in files:
            L.append('--' + LIMIT)
            L.append('Content-Disposition: form-data; name="%s"; filename="%s"' % (key, filename))
            L.append('Content-Type: %s' % get_content_type(filename))
            L.append('')
            L.append(value)
        L.append('--' + LIMIT + '--')
        L.append('')
        body = CRLF.join(L)
        content_type = 'multipart/form-data; boundary=%s' % LIMIT
        return content_type, body

    def get_content_type(self, filename):
        return mimetypes.guess_type(filename)[0] or 'application/octet-stream'
'''
        
        
        
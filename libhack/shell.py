'''
	from pylibhack
	TODO: implement timeout and use subprocess
'''

import os
#import sandbox

class Shell:
	def __init__(self,cmd):
		self.cmd = cmd
		self.out = []
		self.isDebug = False

	def __call__(self, *args, **kwargs):
		params=' '.join(args)+' '
		self.popen(params)
		
	def debug(self):
		self.isDebug = True

	def popen(self,params):
		#sandbox.on()
		if self.isDebug:
			print self.cmd+' '+params
		pd = os.popen(self.cmd+' '+params,'r')
		self.out = []
		for l in pd.readlines():
			self.out.append(l[:-1])
		pd.close()
		#sandbox.off()

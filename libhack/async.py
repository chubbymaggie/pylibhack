# by jo




from threading import Thread


def async(fn,args,daemon=False):
    t = Thread(target=fn,args=args)
    t.setDaemon(daemon)
    t.start()

class Async(Thread):
    def __init__(self,data=None):
        self.data = data
        Thread.__init__(self)
        self.daemon = True
        self.isRunning = False
                
    def stop(self):
        self.isRunning = False
        
    def wait(self):
        try:
            while self.isRunning or self.isAlive():
                self.join(10)
        except KeyboardInterrupt:
            self.stop()
         
class Consumer(Async):
    def push(self,item):
        self.q.push(item)
        
    def parse(self):
        pass
    
    def run(self):
        self.q = self.data
        self.isRunning = True
        self.isReady = True
        
        while self.isRunning:
            item = self.q.get()
            self.isReady = False
            self.parse(item)
            self.isReady = True
            self.q.task_done()
        



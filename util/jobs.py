'''
	Jobs() the Thread pool manager by @sha0coder
'''

import threading
import sys

class AtomicLifo:
	# use import Queue instead of this
	
	def __init__(self,data=[]):
		self.data = data
		self.lock = threading.Lock()

	def split(self,chunks):
		length = len(self.data)
		return [ self.data[i*length // chunks: (i+1)*length // chunks] for i in range(chunks) ]

	def pop(self):
		item = ''
		self.lock.acquire()
		try:
			item = self.data.pop()
		except:
			pass
		self.lock.release()
		return item

	def push(self,item):
		self.lock.acquire() 
		self.data.append(item)
		self.lock.release()

	def reverse(self):
		self.lock.acquire() 
		data = []
		while len(self.data)>0:
			data.append(self.data.pop())
		self.data = data
		self.lock.release()


		
	


class Jobs:
	def __init__(self,data=[]):
		self.jobs = []

	def start(self):
		for j in self.jobs:
			j.daemon = True
			j.start()

	def stop(self):
		for j in self.jobs:
			j.stop()

	def add(self,obj):
		self.jobs.append(obj)

	def wait(self):
		try:
			for j in self.jobs:
				while j.isAlive():
					j.join(8)

		except KeyboardInterrupt:
			print "interrupted."
			sys.exit(1)


#!/usr/bin/env python2
# -*- coding: utf8 -*-

'''
	Easy threads by @sha0coder
'''

from threading import Thread
import time



'''
	Deprecated
	
class Async(Thread):
	def __init__(self,callback,objparam=None):
		self.callback = callback
		self.objparam = objparam
		Thread.__init__(self)
		self.daemon = True
		self.start()
	
	def run(self):
		if self.objparam:
			self.callback(self.objparam)
		else:
			self.callback()
'''

class Async(Thread):
    def __init__(self,data=None):
        self.data = data
        Thread.__init__(self)
        self.daemon = True
        self.isRunning = False
                
    def stop(self):
        self.isRunning = False
        
    def wait(self):
        try:
            while self.isRunning or self.isAlive():
                self.join(10)
        except KeyboardInterrupt:
            self.stop()
         
class Consumer(Async):
    def push(self,item):
        self.q.push(item)
        
    def parse(self):
        pass
    
    def run(self):
        self.q = self.data
        self.isRunning = True
        self.isReady = True
        
        while self.isRunning:
            item = self.q.get()
            self.isReady = False
            self.parse(item)
            self.isReady = True
            self.q.task_done()
        





class Timmer(Thread):
	def __init__(self,time,callback,objparam=None):
		self.time = time
		self.callback = callback
		self.objparam = objparam
		Thread.__init__(self)
		self.daemon = False
		self.start()
	
	def run(self):
		time.sleep(self.time)
		if self.objparam:
			self.callback(self.objparam)
		else:
			self.callback()




def callme(a):
	print "han pasado 10 segundos"

def test():
	Timmer(10,callme,'a')

